import java.util.ArrayList;
import java.util.Arrays;
import java.util.Stack;

public class Tnode {

    public static void main(String[] param) {
        String rpn = "2 5 SWAP -";
        System.out.println("RPN: " + rpn);
        Tnode res = buildFromRPN(rpn);
        System.out.println("Tree: " + res);
        rpn = "2 5 9 ROT - +";
        System.out.println("RPN: " + rpn);
        res = buildFromRPN(rpn);
        System.out.println("Tree: " + res);
        rpn = "2 5 9 ROT + SWAP -";
        System.out.println("RPN: " + rpn);
        res = buildFromRPN(rpn);
        System.out.println("Tree: " + res);
        rpn = "512 1 - 4 * -61 3 / +";
        System.out.println("RPN: " + rpn);
        res = buildFromRPN(rpn);
        System.out.println("Tree: " + res);
    }

    private String name;
    private Tnode firstChild;
    private Tnode nextSibling;

    Tnode(String n, Tnode child, Tnode sibl) {
        name = n;
        firstChild = child;
        nextSibling = sibl;
    }


    // "2 1 - 4 * 6 3 / +" ---> "+(*(-(2,1),4),/(6,3))
    // "2 1 -"  --->  "-(2,1)
    @Override
    public String toString() {
        StringBuffer b = new StringBuffer();
        b.append(getName());
        if (getFirstChild() != null) {
            b.append("(").append(getFirstChild().toString());
        }
        if (getNextSibling() != null) {
            b.append(",").append(getNextSibling().toString()).append(")");
        }
        return b.toString();
    }

    // www.geeksforgeeks.com

    public static Tnode buildFromRPN(String pol) {
        Stack<Tnode> root = new Stack<Tnode>();
        String[] elements = pol.split(" ");

        for (String element : elements) {
            if (isOperator(element)) {
                if (element.trim().toUpperCase().equals("SWAP")){
                    root = swap(root);
                    continue;
                }
                if (element.trim().toUpperCase().equals("ROT")){
                    root = rot(root);
                    continue;
                }
                Tnode secondChild = root.pop();
                Tnode firstChild = root.pop();
                firstChild.setSibling(secondChild);
                root.push(new Tnode(element, firstChild, null));
            } else {
                if (!isNumeric(element))
                    throw new RuntimeException(String.format("The %1$s is not a number neither the operator", element));
                root.push(new Tnode(element, null, null));
            }
        }
        if (root.size() > 1)
            throw new RuntimeException(String.format("The string %1$s is incorrect. Too much nimbers", pol));
        return root.pop();
    }

    public void setSibling(Tnode sibling) {
        nextSibling = sibling;
    }

    public String getName() {
        return name;
    }

    public Tnode getFirstChild() {
        return firstChild;
    }

    public Tnode getNextSibling() {
        return nextSibling;
    }

    public static boolean isNumeric(String s) {
        try {
            int a = Integer.parseInt(s);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isOperator(String s) {
        return (new ArrayList<>(Arrays.asList("-", "/", "+", "*", "SWAP", "ROT"))).contains(s);
    }


    private static Stack<Tnode> swap(Stack<Tnode> root) {
        if (root.size() < 2) throw new IndexOutOfBoundsException();
        Tnode l1 = root.pop();
        Tnode l2 = root.pop();
        root.push(l1);
        root.push(l2);
        return root;
    }

    private static Stack<Tnode> rot(Stack<Tnode> root) {
        if (root.size() < 3) throw new IndexOutOfBoundsException();
        Tnode l1 = root.pop();
        Tnode l2 = root.pop();
        Tnode l3 = root.pop();
        root.push(l2);
        root.push(l1);
        root.push(l3);
        return root;
    }
}

